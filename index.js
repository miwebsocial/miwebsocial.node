#!/usr/bin/env node

var app = require('koa')(),
    session = require('koa-session'),
    cookie = require('cookie');
    //serve = require('koa-static');

const PORT = 9876;
const IP = '0.0.0.0';

app.keys = ['465fsa654asf12'];
app.use(session(app));

app.use(function *(next) {
  if (typeof this.session.name === 'undefined') {
    this.session.name = Math.round(Math.random() * 10000);
  }
  yield next;
});

//app.use(serve('./public'));

var server = require('http').Server(app.callback()),
    io = require('socket.io')(server);

io.set("authorization", function(data, accept) {        
    //console.log(data.headers.cookie);
    if (data.headers.cookie && data.headers.cookie.indexOf('io') > -1) {
        data.cookie = cookie.parse(data.headers.cookie)['io'];    
        //data.name = JSON.parse(new Buffer(data.cookie, 'base64')).name;
    } else {
        //return accept('No cookie transmitted.', false);
        console.log("Error: No cookie transmitted.");
    }
    accept(null, true);
    console.log("conexión ok");
});

//var socket = io.listen(9876);

io.on('connection', function(client) {
    /*console.log("Cookie:", client.handshake.headers.cookie);
    if(typeof client.handshake.headers.cookie === 'undefined')
        client.emit('msg_system', {'type': 'msg', 'code': 'CONN_NO_COOKIE'}); */
    
    
    client.on('room', function(room) {
        console.log("usando room: "+room);
        if(client.room)
            client.leave(socket.room);

        client.room = room;
        client.join(client.room);

        client.emit('msg_system', {'type': 'msg', 'code': 'CONN_OK'});                
        
    });

    client.on('message', function(message) {
        console.log('Mensaje en room ' + client.room, message);
                
        //Se añade a pending
        //pendingMessagesForSocket[message.content.id] = message;        
        client.broadcast.to(client.room).emit('message', message);        
    });

    client.on('disconnect', function() {
        console.log('El cliente se desconectó');
    });
});
server.listen(PORT, IP);